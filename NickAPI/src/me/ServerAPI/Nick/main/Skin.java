package me.ServerAPI.Nick.main;

import java.io.IOException;
import java.util.Collection;
import java.util.Properties;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;

public class Skin {
	
	public static void change(CraftPlayer cp, String SkinName) {
		
		GameProfile gp = cp.getProfile();
		
		try {
			gp = GameProfileBuilder.fetch(UUIDFetcher.getUUID(SkinName));
		} catch (IOException e) {
			System.out.println("DER SKIN KONNTE NICHT GELADEN WERDEN!");
			e.printStackTrace();
			return;
		} 
		
		Collection<Property> props = gp.getProperties().get("textures");
		cp.getProfile().getProperties().removeAll("textures");
		cp.getProfile().getProperties().putAll("textures",props);
		PacketPlayOutEntityDestroy destroy = new PacketPlayOutEntityDestroy(cp.getEntityId());
		sendPacket(destroy);
		PacketPlayOutPlayerInfo info = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, cp.getHandle());
		sendPacket(info);
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				PacketPlayOutPlayerInfo addinfo = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, cp.getHandle());
				sendPacket(addinfo);
				PacketPlayOutNamedEntitySpawn spawn = new PacketPlayOutNamedEntitySpawn(cp.getHandle());
				for(Player all : Bukkit.getOnlinePlayers()) {
					if(!all.getName().equals(cp.getName())) {
					((CraftPlayer) all).getHandle().playerConnection.sendPacket(spawn);
				}
				}
				
				
				
			}
		}.runTaskLater(Main.getPlugin(Main.class), 4);
		
		
	}
	
	public static void sendPacket(Packet packet) {
		
		for(Player all : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) all).getHandle().playerConnection.sendPacket(packet);
		}
 		
	}

}
