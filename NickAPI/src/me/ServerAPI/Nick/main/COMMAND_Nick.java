package me.ServerAPI.Nick.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class COMMAND_Nick implements CommandExecutor {

	public static List<String> nicks = new ArrayList<>();
	
	@Override
	public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
		Player p = (Player) s;
		if(p.hasPermission("nick.nick")) {
			if(NickAPI.nickedPlayers.contains(p.getUniqueId())) {
				NickAPI.unnick(p);
				p.sendMessage(Main.pf+"�4Dein Nickname wurde entfernt�8!");
			} else {
				nicks.add("Scrandle");
				nicks.add("HerrWookie");
				nicks.add("GommeHD");
				nicks.add("ungespielt");
				nicks.add("DerBoss");
				nicks.add("haze");
				nicks.add("WlanParty");
				nicks.add("Lontix");
				nicks.add("Doodle_Fan");
				nicks.add("Freepy");
				
				Random rdm = new Random();
				int zufallszahl = rdm.nextInt(nicks.size());
				String nickname = nicks.get(zufallszahl);
				
				NickAPI.Nick(p, nickname);
				Skin.change(((CraftPlayer)p), nickname);
				p.sendMessage(" ");
				p.sendMessage(Main.pf+"�4Dein aktueller Nickname lautet�8: �6"+p.getName());
				p.sendMessage(" ");
			}
			
		}
		
		
		
		
		return true;
	}

}
