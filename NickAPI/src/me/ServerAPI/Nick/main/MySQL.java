package me.ServerAPI.Nick.main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;

public class MySQL {
	
	public static String username;
	public static String password;
	public static String database;
	public static String host;
	public static String port;
	public static Connection con;
	
	public static void Connect(){
		if(!isConnected()){
			try {
				con = DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+database, username, password);
				Bukkit.getConsoleSender().sendMessage(Main.pf+"žaVerbindung erfolgreich hergestellt");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
	}
	
	public static void close(){
		if(isConnected()){
			
			try {
				con.close();
				Bukkit.getConsoleSender().sendMessage(Main.pf+"žaDie MySql verbindung, wurde erfolgreich beendet!");
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		
		
	}
	
		public static void update(String qry){
			if(isConnected()){
				try {
					con.createStatement().executeUpdate(qry);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		public static ResultSet getResult(String qry){
			if(isConnected()){
				try {
					return con.createStatement().executeQuery(qry);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return null;
			
		
	}
	
	public static boolean isConnected(){
		return con != null;
		
	}
	public static void CreateTable(){
		if(isConnected()){
		try {
			con.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Nick(Spielername VARCHAR(100), UUID VARCHAR(100), Ja VARCHAR(100))");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
		
		
	}
	

}
