package me.ServerAPI.Nick.main;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.mojang.authlib.GameProfile;
import com.mysql.jdbc.Field;

import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;

public class NickAPI {
	
	public static HashMap<UUID, String> infos = new HashMap<>(); 
	public static List<UUID> nickedPlayers = new ArrayList<>();
	
	public static void Nick(Player p, String nickname) {
		nickedPlayers.add(p.getUniqueId());
		infos.put(p.getUniqueId(), p.getName());
		PacketPlayOutPlayerInfo playerinfo = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.UPDATE_DISPLAY_NAME, ((CraftPlayer)p).getHandle());
		
		for(Player all : Bukkit.getOnlinePlayers()) {
			((CraftPlayer)all).getHandle().playerConnection.sendPacket(playerinfo);
		}
		
		GameProfile profile = ((CraftPlayer)p).getProfile();
		try {
			java.lang.reflect.Field nameField = profile.getClass().getDeclaredField("name");
			nameField.setAccessible(true);
			int modifiers = nameField.getModifiers();
			java.lang.reflect.Field modifierField = nameField.getClass().getDeclaredField("modifiers");
			modifiers = modifiers & ~Modifier.FINAL;
			modifierField.setAccessible(true);
			nameField.set(profile, nickname);
			
		} catch (SecurityException | NoSuchFieldException | IllegalAccessException e) {
			System.out.println("FEHLER");
		}
		Skin.change(((CraftPlayer)p), nickname);
		p.setDisplayName("�6"+nickname);
		p.setPlayerListName("�6"+nickname);
		
		
		
	}
	
	public static void unnick (Player p) {
		String nickname = infos.get(p.getUniqueId());
		nickedPlayers.remove(p.getUniqueId());
		infos.remove(p.getUniqueId());
		PacketPlayOutPlayerInfo playerinfo = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.UPDATE_DISPLAY_NAME, ((CraftPlayer)p).getHandle());
		
		for(Player all : Bukkit.getOnlinePlayers()) {
			((CraftPlayer)all).getHandle().playerConnection.sendPacket(playerinfo);
		}
		
		GameProfile profile = ((CraftPlayer)p).getProfile();
		try {
			java.lang.reflect.Field nameField = profile.getClass().getDeclaredField("name");
			nameField.setAccessible(true);
			int modifiers = nameField.getModifiers();
			java.lang.reflect.Field modifierField = nameField.getClass().getDeclaredField("modifiers");
			modifiers = modifiers & ~Modifier.FINAL;
			modifierField.setAccessible(true);
			nameField.set(profile, nickname);
			
			
		} catch (SecurityException | NoSuchFieldException | IllegalAccessException e) {
			System.out.println("FEHLER");
		}
		Skin.change(((CraftPlayer)p), nickname);
		p.setDisplayName("�f"+nickname);
		p.setPlayerListName("�f"+nickname);
		
		
	}

}
