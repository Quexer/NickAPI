package me.ServerAPI.Nick.main;

import java.util.List;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;

public class ItemUtils {
	
	public static ItemStack createItem(Material m, int Count, String Name) {
		ItemStack is = new ItemStack(m, Count);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(Name);
		is.setItemMeta(meta);
		return is;
	}
	
	public static ItemStack createItem(Material m, int Count, int subid, String Name) {
		ItemStack is = new ItemStack(m, Count, (short) subid);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(Name);
		is.setItemMeta(meta);
		return is;
	}
	
	public static ItemStack createSkull(String name, String owner) {
		ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
		SkullMeta meta = (SkullMeta) is.getItemMeta();
		meta.setOwner(owner);
		meta.setDisplayName(name);
		is.setItemMeta(meta);
		return is;
	}
	
	public static ItemStack createSkullLore(String name, String owner, List<String> lore) {
		ItemStack is = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
		SkullMeta meta = (SkullMeta) is.getItemMeta();
		meta.setOwner(owner);
		meta.setDisplayName(name);
		meta.setLore(lore);
		is.setItemMeta(meta);
		return is;
	}
	
	public static ItemStack createItem(Material m, int Count, String Name, List<String> lore) {
		ItemStack is = new ItemStack(m, Count);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(Name);
		meta.setLore(lore);
		is.setItemMeta(meta);
		return is;
	}
	
	public static ItemStack createItem(Material m, int Count, int i, String Name, Enchantment e, int el) {
		ItemStack is = new ItemStack(m, Count, (short) i);
		if (Name != null) {
			ItemMeta im = is.getItemMeta();
			im.setDisplayName(Name);
			is.setItemMeta(im);
		}
		ItemMeta im = is.getItemMeta();
		im.addEnchant(e, el, true);
		is.setItemMeta(im);
		return is;
	}
	
    public static ItemStack setEnchanted(ItemStack item){
        net.minecraft.server.v1_8_R3.ItemStack s = CraftItemStack.asNMSCopy(item);
        NBTTagCompound t = null;
        if (!s.hasTag()) {
            t = new NBTTagCompound();
            s.setTag(t);
        }
        if (t == null) t = s.getTag();
        NBTTagList e = new NBTTagList();
        t.set("ench", e);
        s.setTag(t);
        return CraftItemStack.asCraftMirror(s);
    }
    
    public static ItemStack createBoot(Material m, int Count, String Name, Color Color) {
		ItemStack is = new ItemStack(m, Count);
		LeatherArmorMeta meta = (LeatherArmorMeta) is.getItemMeta();
		meta.setDisplayName(Name);
		meta.setColor(Color);
		is.setItemMeta(meta);
		return is;
	}

	
    
	
}
